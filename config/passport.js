var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var User = require('../models/user');
var md5 = require('md5');

passport.use(new LocalStrategy({
        usernameField: 'login'
    },
    function(username, password, done) {
        User.findOne({ login: username }, function (err, user) {
            if (err) { return done(err); }
            if (!user) {
                return done(null, false, {
                    message: 'Incorrect login.'
                });
            }
            if (user.password !== md5(password)) {
                return done(null, false, {
                    message: 'Incorrect password.'
                });
            }
            return done(null, user);
        });
    }
));
