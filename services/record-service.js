var Record = require('../models/record');

module.exports.createRecord = function (body) {
    return Record.create({
        patient_id: body.patient_id,
        doctor_id: body.doctor_id,
        date: body.date,
        time: body.time,
        materials: body.materials,
        status: body.status
    });
};


module.exports.removeRecord = function (id) {
    return Record.remove({_id: id}).then(function (res) {
        return res;
    });
};

module.exports.getRecords = function (id) {
    return Record.find({patient_id: id})
        .then(function(record) {
            return record;
        });
};

module.exports.getRecordsByPatientId = function (id) {
    return Record.find({patient_id: id})
        .then(function(record) {
            return record.reverse();
        });
};

module.exports.getAllRecords = function () {
    return Record.find()
        .then(function(records) {
            return records;
        });
};


module.exports.getMyRecords = function (id, date) {
    return Record.find({doctor_id: id, date: date})
        .then(function(record) {
            return record;
        });
};
