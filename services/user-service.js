var User = require('../models/user');
var Patient = require('../models/patient');
var Doctor = require('../models/doctor');
var Registrator = require('../models/registrator');
var md5 = require('md5');

module.exports.saveUser = function (user) {
    if (user.role === 'doctor') {
        return User.create({
            login: user.login,
            password: md5(user.password),
            role: user.role
        }).then(function (userTab) {
            Doctor.create({
                user: userTab,
                name: user.name,
                email: user.login,
                specialty: user.specialty
            });
        });
    }
    if (user.role === 'patient') {
        return User.create({
            login: user.login,
            password: md5(user.password),
            role: user.role
        }).then(function (userTab) {
            Patient.create({
                user: userTab,
                name: user.name,
                email: user.login
            });
        });
    }
    if (user.role === 'registrator') {
        return User.create({
            login: user.login,
            password: md5(user.password),
            role: user.role
        }).then(function (userTab) {
            Registrator.create({
                user: userTab,
                name: user.name,
            });
        });
    }
};

module.exports.getUserInfo = function (id, role) {
    if (role === 'patient') {
        return Patient.findOne({user: id}).populate('user')
            .then(function(patientInfo) {
                return patientInfo;
            });
    } else if (role === 'doctor') {
        return Doctor.findOne({user: id}).populate('user')
            .then(function(doctorInfo) {
                return doctorInfo;
            });
    } else {
        return Registrator.findOne({user: id}).populate('user')
            .then(function(registratorInfo) {
                return registratorInfo;
            });
    }
};






