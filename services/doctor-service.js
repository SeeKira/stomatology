var Doctor = require('../models/doctor');


module.exports.getDoctors = function () {
    return Doctor.find().populate('user')
        .then(function(doctors) {
            return doctors;
        });
};

module.exports.getDoctorInfo = function (id) {
    return Doctor.findOne({user: id}).populate('user')
        .then(function(doctorInfo) {
            return doctorInfo;
        });
};
