var VisitCard = require('../models/visit_card');
var Record = require('../models/record');

module.exports.saveVisitCard = function (body) {
    return Record.findOne({_id: body.record}).then(function (record) {
        return VisitCard.create({
            record: record,
            complaints: body.complaints,
            allergic_history: body.allergic_history,
            postponed_diseases: body.postponed_diseases,
            medications_taken: body.medications_taken,
            thermometry: body.thermometry,
            tonometry: body.tonometry,
            development_disease: body.development_disease,
            visual_inspection: body.visual_inspection,
            examination: body.examination,
            dentition: body.dentition,
            bite: body.bite,
            hygiene_index: body.hygiene_index,
            index_kpu: body.index_kpu,
            preliminary_diagnosis: body.preliminary_diagnosis,
            code_mkb: body.code_mkb,
            survey_plan: body.survey_plan,
            survey_data: body.survey_data,
            clinical_diagnosis: body.clinical_diagnosis,
            code_mkb_second: body.code_mkb_second,
            treatment_plan: body.treatment_plan,
            treatment_protocol: body.treatment_protocol
        });
    });
};

module.exports.getVisitCard = function (id) {
    return VisitCard.find({record: id}).populate('Record')
        .then(function(card) {
            return card;
        });
};
