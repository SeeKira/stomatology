var Patient = require('../models/patient');
var User = require('../models/user');


module.exports.savePatientInfo = function (id, body) {

    return Patient.findOne({user: id})
        .then(function(user) {
            if (body.birthday != null) {
                user.birthday = body.birthday;
            }

            if (body.sex != null) {
                user.sex = body.sex;
            }

            if (body.slid != null) {
                user.slid = body.slid;
            }

            if (body.phone != null) {
                user.phone = body.phone;
            }


            if (body.address != null) {
                user.address = body.address;
            }

            if (body.passport != null) {
                user.passport.name = body.passport.name;
                user.passport.series = body.passport.series;
                user.passport.number = body.passport.number;
                user.passport.date = body.passport.date;
                user.passport.issued_by = body.passport.issued_by;
            }

            if (body.policy != null) {
                user.policy.series = body.policy.series;
                user.policy.number = body.policy.number;
            }

            if (body.code != null) {
                user.code = body.code;
            }

            if (body.guardian != null) {
                user.guardian.name = body.guardian.name;
                user.guardian.phone = body.guardian.phone;
            }

            Patient.update({user: id}, user).populate('user').then(function (updatedUserInfo){

            });
            return user;
        });
};

module.exports.getPatientInfo = function (id) {
    return Patient.findOne({user: id}).populate('user')
        .then(function(patientInfo) {
            return patientInfo;
        });
};

module.exports.getPatients = function () {
    return Patient.find().populate('user')
        .then(function(patients) {
            return patients;
        });
};
