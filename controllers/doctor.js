var DoctorService = require('../services/doctor-service');

module.exports.getDoctors =  function(req, res) {
    DoctorService.getDoctors()
        .then(function(doctors) {
            res.json(doctors);
        });
};

module.exports.getDoctorInfo =  function(req, res) {
    DoctorService.getDoctorInfo(req.params.id)
        .then(function(doctorInfo) {
            res.json(doctorInfo);
        });
};
