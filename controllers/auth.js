var passport = require('passport');
var jwt = require('jsonwebtoken');
require('../config/passport');
var User = require('../models/user');

var sendJSONresponse = function(res, status, content) {
    res.status(status);
    res.json(content);
};


module.exports.login = function(req, res) {

    passport.authenticate('local', function(err, user, info){
        var token;
        var refreshToken;
        var decoded;

        if (err) {
            sendJSONresponse(res, 404, err);
            return;
        }

        if(user){
            token = user.generateJwt();
            decoded = jwt.decode(token);
            refreshToken = user.generateRefreshJwt();
            user.refreshToken = refreshToken;
            User.update({_id: user.id}, user).then(function (){});
            sendJSONresponse(res, 200, {
                "token": token,
                "refreshToken": refreshToken,
                "exp": decoded.exp
            });
        } else {
            sendJSONresponse(res, 401, info);
        }
    })(req, res);
};

module.exports.refresh = function(req, res) {
    var refreshToken = req.body.refreshToken;
    var decoded = jwt.decode(refreshToken);
    User.findOne({_id: decoded._id})
        .then(function(user) {
            if (user.refreshToken === refreshToken) {
                var token = user.generateJwt();
                var decoded = jwt.decode(token);
                var newRefreshToken = user.generateRefreshJwt();
                user.refreshToken = newRefreshToken;
                User.update({_id: user._id}, user).then(function (){});
                sendJSONresponse(res, 200, {
                    "token": token,
                    "refreshToken": newRefreshToken,
                    "exp": decoded.exp
                });
            } else {
                sendJSONresponse(res, 401, {
                    message: 'tokens do not match'
                });
            }
        });
};
