var PatientService = require('../services/patient-service');


module.exports.savePatientInfo =  function(req, res) {
    PatientService.savePatientInfo(req.body.id, req.body)
        .then(function(patientInfo) {
            res.json(patientInfo);
        });
};

module.exports.getPatientInfo =  function(req, res) {
    PatientService.getPatientInfo(req.params.id)
        .then(function(patientInfo) {
            res.json(patientInfo);
        });
};


module.exports.getPatients =  function(req, res) {
    PatientService.getPatients()
        .then(function(patientInfo) {
            res.json(patientInfo);
        });
};
