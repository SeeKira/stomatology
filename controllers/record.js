var RecordService = require('../services/record-service');


module.exports.createRecord =  function(req, res) {
    RecordService.createRecord(req.body)
        .then(function(record) {
            res.json(record);
        });
};

module.exports.removeRecord =  function(req, res) {
    RecordService.removeRecord(req.body.id)
        .then(function(record) {
            res.json(record);
        });
};

module.exports.getRecords =  function(req, res) {
    RecordService.getRecords(req.payload._id)
        .then(function(records) {
            res.json(records);
        });
};

module.exports.getRecordsByPatientId =  function(req, res) {
    RecordService.getRecordsByPatientId(req.params.id)
        .then(function(records) {
            res.json(records);
        });
};


module.exports.getAllRecords =  function(req, res) {
    RecordService.getAllRecords()
        .then(function(records) {
            res.json(records);
        });
};

module.exports.getMyRecords =  function(req, res) {
    RecordService.getMyRecords(req.payload._id, req.params.date)
        .then(function(records) {
            res.json(records);
        });
};
