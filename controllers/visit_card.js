var VisitCardService = require('../services/visit_card-service');


module.exports.saveVisitCard =  function(req, res) {
    VisitCardService.saveVisitCard(req.body)
        .then(function(card) {
            res.json(card);
        });
};

module.exports.getVisitCard =  function(req, res) {
    VisitCardService.getVisitCard(req.params.id)
        .then(function(card) {
            res.json(card);
        });
};
