var UserService = require('../services/user-service');
var User = require('../models/user');
var jwt = require('jsonwebtoken');
var passport = require('passport');
require('../config/passport');

var sendJSONresponse = function(res, status, content) {
    res.status(status);
    res.json(content);
};

module.exports.register =  function(req, res) {
    UserService.saveUser(req.body)
        .then(function () {
            passport.authenticate('local', function(err, user, info){
                var token;
                var refreshToken;
                var decoded;

                if (err) {
                    sendJSONresponse(res, 404, err);
                    return;
                }

                if(user){
                    token = user.generateJwt();
                    decoded = jwt.decode(token);
                    refreshToken = user.generateRefreshJwt();
                    user.refreshToken = refreshToken;
                    User.update({_id: user.id}, user).then(function (){});
                    sendJSONresponse(res, 200, {
                        "token": token,
                        "refreshToken": refreshToken,
                        "exp": decoded.exp
                    });
                } else {
                    sendJSONresponse(res, 401, info);
                }
            })(req, res);
        });
};

module.exports.getUserInfo =  function(req, res) {
    UserService.getUserInfo(req.payload._id, req.payload.role)
        .then(function(userInfo) {
            res.json(userInfo);
        });
};




