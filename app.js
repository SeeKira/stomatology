var express = require('express');
var http = require('http');
var fs = require('fs');
var app = express();
var bodyParser = require('body-parser');
var router = require('./routes/index');
var mongoose = require('mongoose');
var fileUpload = require('express-fileupload');
var cors = require('cors');
var passport = require('passport');


//////////////////////////////////////////////////////////////
app.use('/public', express.static(__dirname + '/public'));

app.use(fileUpload());
app.use(cors());
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));

mongoose.connect('mongodb://localhost/stomatology');

passport.serializeUser(function(user, done) {
    done(null, user.id);
});

passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
        done(err, user);
    });
});


app.use('/', router);

app.get('/',function(req,res){
    res.sendFile(__dirname + '/public/index.html');
});

app.get('/img/:filename', function(req, res) {
    res.sendFile(__dirname + '/public/img/' + req.params.filename);
});

app.get('/files/:filename', function(req, res) {
    res.sendFile(__dirname + '/public/files/' + req.params.filename);
});

app.listen(3334, function () {
    console.log('Example app listening on port 3334!');
});
