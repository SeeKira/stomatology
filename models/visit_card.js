var mongoose = require("mongoose");

var visitCardSchema = mongoose.Schema({
    record: {type: mongoose.Schema.Types.ObjectId, ref: 'Record'},
    complaints: String,
    allergic_history: String,
    postponed_diseases: String,
    medications_taken: String,
    thermometry: String,
    tonometry: String,
    development_disease: String,
    visual_inspection: String,
    examination: String,
    dentition: String,
    bite: String,
    hygiene_index: String,
    index_kpu: String,
    preliminary_diagnosis: String,
    code_mkb: String,
    survey_plan: String,
    survey_data: String,
    clinical_diagnosis: String,
    code_mkb_second: String,
    treatment_plan: String,
    treatment_protocol: String
});

var visit_card = mongoose.model('Visit_card', visitCardSchema);

module.exports = visit_card;
