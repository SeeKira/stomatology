var mongoose = require("mongoose");

var doctorSchema = mongoose.Schema({
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    name: String,
    email: String,
    specialty: String,
});

var doctor = mongoose.model('Doctor', doctorSchema);

module.exports = doctor;
