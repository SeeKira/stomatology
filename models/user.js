var mongoose = require("mongoose");
var jwt = require('jsonwebtoken');
var mongooseHidden = require('mongoose-hidden')();

var usersSchema = mongoose.Schema({
    login: { type: String, unique: true},
    password: { type: String, hide: true },
    role: String,
    refreshToken: String
});

usersSchema.plugin(mongooseHidden, { hidden: { _id: false, password: true } });

usersSchema.methods.generateJwt = function() {
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 7);

    return jwt.sign({
        _id: this._id,
        login: this.login,
        role: this.role,
        exp: parseInt(expiry.getTime() / 1000)
    }, "comp");
};

usersSchema.methods.generateRefreshJwt = function() {
    var expiry = new Date();
    expiry.setDate(expiry.getDate() + 60);

    return jwt.sign({
        _id: this._id,
        exp: parseInt(expiry.getTime() / 1000)
    }, "comp");
};

var users = mongoose.model('User', usersSchema);

module.exports = users;
