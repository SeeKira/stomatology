var mongoose = require("mongoose");

var recordSchema = mongoose.Schema({
    patient_id: String,
    doctor_id: String,
    date: String,
    time: String,
    materials: Array,
    status: String
});

var record = mongoose.model('Record', recordSchema);

module.exports = record;
