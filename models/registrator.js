var mongoose = require("mongoose");

var registratorSchema = mongoose.Schema({
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    name: String
});

var registrator = mongoose.model('Registrator', registratorSchema);

module.exports = registrator;
