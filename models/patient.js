var mongoose = require("mongoose");

var patientSchema = mongoose.Schema({
    user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
    name: String,
    birthday: String,
    sex: String,
    slid: String,
    phone: String,
    email: String,
    address: String,
    passport: {
        name: String,
        series: String,
        number: String,
        date: String,
        issued_by: String
    },
    policy: {
        series: String,
        number: String
    },
    code: String,
    guardian: {
        name: String,
        phone: String,
    }
});

var patient = mongoose.model('patient', patientSchema);

module.exports = patient;
