var express = require('express');
var router = express.Router();
var jwt = require('express-jwt');
var ctrlUsers = require('../controllers/users');
var ctrlPatient = require('../controllers/patient');
var ctrlDoctor = require('../controllers/doctor');
var ctrlRecord = require('../controllers/record');
var ctrlVisitCard = require('../controllers/visit_card');

var auth = jwt({
    secret: "comp",
    userProperty: 'payload'
});

var ctrlAuth = require('../controllers/auth');

router.post('/login', ctrlAuth.login);
router.post('/auth/refresh-token', ctrlAuth.refresh);
router.post('/register', ctrlUsers.register);

router.get('/getUserInfo', auth, ctrlUsers.getUserInfo);

router.post('/savePatientInfo', auth, ctrlPatient.savePatientInfo);
router.get('/getPatientInfo/:id', auth, ctrlPatient.getPatientInfo);

router.post('/createRecord', auth, ctrlRecord.createRecord);
router.get('/getRecords', auth, ctrlRecord.getRecords);
router.get('/getRecordsByPatientId/:id', auth, ctrlRecord.getRecordsByPatientId);
router.get('/getAllRecords', auth, ctrlRecord.getAllRecords);
router.get('/getMyRecords/:date', auth, ctrlRecord.getMyRecords);
router.post('/removeRecord', auth, ctrlRecord.removeRecord);

router.get('/getPatients', auth, ctrlPatient.getPatients);

router.get('/getDoctors', auth, ctrlDoctor.getDoctors);
router.get('/getDoctorInfo/:id', auth, ctrlDoctor.getDoctorInfo);

router.post('/saveVisitCard', auth, ctrlVisitCard.saveVisitCard);
router.get('/getVisitCard/:id', auth, ctrlVisitCard.getVisitCard);




module.exports = router;
